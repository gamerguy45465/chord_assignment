package main

import (
	"bufio"
	"context"
	"crypto/sha1"
	"flag"
	"fmt"
	"log"
	"math/big"
	"math/rand"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	"google.golang.org/grpc"

	"cs3410/chord/protocol"
	pb "cs3410/chord/protocol"
)

const (
	defaultPort       = "8080"
	seccessorListSize = 3
	keySize           = sha1.Size * 8
	maxLookupSteps    = 32
)

var (
	two          = big.NewInt(2)
	hashMod      = new(big.Int).Exp(big.NewInt(2), big.NewInt(keySize), nil)
	localaddress string
)

type Node struct {
	pb.UnimplementedChordServer
	mu          sync.RWMutex
	Address     string
	FingerTable []string
	Predecessor string
	Successors  []string

	Bucket map[string]string
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**********************************Getters*************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

func (n *Node) SuccessorsSlice(ctx context.Context, reg *pb.Domain) (*pb.Range, error) {
	var ret string
	for i := 0; i < len(n.Successors); i++ {
		ret += n.Successors[i]
	}

	return &pb.Range{Value: ret}, nil
}

func getSuccessorSlice(ctx context.Context, id string) {
	conn, err := grpc.Dial(id, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("No")
	}
	defer conn.Close()
	client := pb.NewChordClient(conn)

	ret, _ := client.SuccessorsSlice(ctx, &pb.Domain{})

	fmt.Println(ret)

}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**********************************PING****************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

func (n *Node) Ping(ctx context.Context, reg *pb.PingRequest) (*pb.PingResponse, error) {
	return &pb.PingResponse{}, nil
}

func ping(ctx context.Context, addr string) error {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())

	if err != nil {
		return err
	}

	defer conn.Close()

	client := pb.NewChordClient(conn)

	_, err1 := client.Ping(ctx, &pb.PingRequest{})

	fmt.Println(err1)

	return err1
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**********************************GET*****************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

func (n *Node) Get(ctx context.Context, req *pb.GetRequest) (*pb.GetResponse, error) {
	n.mu.RLock()
	defer n.mu.RUnlock()
	value, exists := n.Bucket[req.GetKey()]
	if !exists {
		return &pb.GetResponse{Value: ""}, nil
	}
	return &pb.GetResponse{Value: value}, nil
}

func get(ctx context.Context, key string, addr string) (string, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return "", err
	}
	defer conn.Close()

	client := pb.NewChordClient(conn)

	response, err := client.Get(ctx, &pb.GetRequest{Key: key})
	if err != nil {
		return "", err
	}

	return response.GetValue(), nil
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

func (n *Node) Put(ctx context.Context, req *pb.PutRequest) (*pb.PutResponse, error) {
	n.mu.Lock()
	defer n.mu.Unlock()
	n.Bucket[req.GetKey()] = req.GetValue()
	return &pb.PutResponse{}, nil

}

func put(ctx context.Context, key, value string, addr string) error {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer conn.Close()

	client := pb.NewChordClient(conn)

	_, err = client.Put(ctx, &pb.PutRequest{Key: key, Value: value})
	return err
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**********************************DUMP****************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

func (n *Node) Dump(ctx context.Context, in *pb.DumpRequest) (*pb.DumpResponse, error) {
	n.mu.RLock()
	defer n.mu.RUnlock()

	log.Println()
	log.Print("Information about this node")

	log.Print("Neighborhood")
	log.Print("self:   ", n.Predecessor)
	log.Print("self:   ", n.Address)
	for i, succ := range n.Successors {
		log.Printf("succ %d: %s", i, succ)
	}
	log.Println()

	return &pb.DumpResponse{}, nil
}

func dumpRPC(id string, ctx context.Context) {
	conn, err := grpc.Dial(id, grpc.WithInsecure())

	if err != nil {
		log.Print("Error: Unable to dump core")
		return
	}
	defer conn.Close()
	client := pb.NewChordClient(conn)

	client.Dump(ctx, &pb.DumpRequest{})
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/********************************DELETE****************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

func (n *Node) Delete(ctx context.Context, req *pb.DeleteRequest) (*pb.DeleteResponse, error) {
	n.mu.Lock()
	defer n.mu.Unlock()
	delete(n.Bucket, req.GetKey())
	return &pb.DeleteResponse{}, nil
}

func delete_rpc(ctx context.Context, key string, addr string) error {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer conn.Close()

	client := pb.NewChordClient(conn)

	_, err = client.Delete(ctx, &pb.DeleteRequest{Key: key})
	return err
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**********************************SHELL***************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

func shell(node *Node, port string, joinaddress string) {
	log.Println()
	log.Print("Welcome! Please type \"help\" for help with commands")
	log.Println()

	ctx := context.Background()
	in := bufio.NewScanner(os.Stdin)

	for {

		for in.Scan() {
			var breakme bool = false
			words := strings.Fields(in.Text())
			if len(words) == 0 {
				continue
			}

			switch words[0] {
			case "port":
				if len(words) == 1 {
					log.Print("current port: ", port)
				} else if len(words) > 2 {
					log.Print("Please supply a port number, or omit it to report current setting")
				} else if node != nil {
					log.Print("error, cannot change port after joining a ring")
				} else {
					port = words[1]
					log.Print("set port to ", port)
				}

			case "create":
				if len(words) > 2 {
					log.Print("error, optional port is the only argument allowed")
				} else if node != nil {
					log.Print("error, already part of a ring; use quit to get out")
				} else {
					if len(words) == 2 {
						port = words[1]
						log.Print("port set to ", port)
					}
					var err error
					fmt.Println("\n\n\n\n\n\n\n", port, "\n\n\n\n\n\n\n\n\n\n")
					if node, err = StartServer(port, "", ctx); err != nil {
						//if node, err = Create(port, ctx); err != nil {
						log.Print("error, failed to create node: ", err)
						node = nil
					}
				}
			case "join":
				if len(words) > 2 {
					log.Print("error, join address is the only argument expected")
				} else if joinaddress == "" && len(words) == 1 {
					log.Print("error, must supply address of existing ring")
				} else {
					if len(words) == 2 {
						joinaddress = words[1]
						fmt.Println(joinaddress, "0")
					}
					if strings.HasPrefix(joinaddress, ":") {
						joinaddress = net.JoinHostPort(localaddress, joinaddress[1:])
						fmt.Println(joinaddress, "1")
					} //else if !strings.Contains(joinaddress, ":") {
					//joinaddress = net.JoinHostPort(joinaddress, port)
					//fmt.Println(joinaddress, "2")
					//}

					//address, _ := find(hashString(localaddress), localaddress)

					//if succ == joinaddress {
					var err error
					if node, err = StartServer(port, joinaddress, ctx); err != nil {
						//if node, err = Join(joinaddress, port, ctx); err != nil {
						log.Print("error, failed to join node: ", err)
						node = nil
					}

					//}

				}
			case "dump":
				if len(words) > 1 {
					log.Print("error, no arguments expected")
				} else if node == nil {
					log.Print("error, not a member of a ring")
				} else {
					dumpRPC(node.Address, ctx)
				}

			case "put":
				if len(words) != 4 {
					log.Print("error, must supply key, value, address, and nothing else")
				} else if node == nil {
					log.Print("error, not a member of a ring")
				} else {
					key, value, addre := words[1], words[2], words[3]
					//key, value, prev := words[1], words[2], words[3]
					//key, value := words[1], words[2]
					//isTrue, addr := node.find_successor(prev)
					//if isTrue {
					log.Print("sending request to node ", addre)
					err := put(ctx, key, value, addre)
					if err != nil {
						log.Print("error: ", err)
					}

					//} else {
					//	log.Print("Error, could not assign node")
					//}
				}

			case "get":
				if len(words) != 3 {
					log.Print("error, must supply key, address, and nothing else")
				} else if node == nil {
					log.Print("error, not a member of a ring")
				} else {
					key, addre := words[1], words[2]
					//key, prev := words[1], words[2]
					//isTrue, addr := node.find_successor(prev)
					//if isTrue {
					log.Print("sending request to node ", addre)
					value, err := get(ctx, key, addre)
					if err != nil {
						log.Print("error in remote call: ", err)
					}
					if value == "" {
						log.Print("not found")
					} else {
						log.Print("found ", key, " => ", value)
					}

					//} else {
					//	log.Print("Error, could not find node")
					//	}
				}
			case "delete":
				if len(words) != 3 {
					log.Print("error, must supply key, address, and nothing else")
				} else if node == nil {
					log.Print("error, must suppy key, address, and nothing else")
				} else {
					key, addre := words[1], words[2]
					//key, prev := words[1], words[2]
					//key := words[1]
					//isTrue, addr := node.find_successor(prev)
					//if isTrue {
					log.Print("sending delete request to node ", addre)
					err := delete_rpc(ctx, key, addre)
					if err != nil {
						log.Print("error: ", err)
					}

				} //else {
				//log.Print("Error, node is not there")
				//}

			case "ping":
				if len(words) != 2 {
					log.Print("error, must supply ping address")
				} else {
					addr := words[1]
					log.Print("sending ping request")
					err := ping(ctx, addr)
					if err != nil {
						log.Print("Error: ", err)
					} else {
						log.Print("Ping successful")
					}
				}

			case "list":
				if len(words) != 2 {
					log.Print("error, must supply address, and nothing else")
				} else if node == nil {
					log.Print("error, not a member of a ring")
				} else {
					addre := words[1]
					//key, prev := words[1], words[2]
					//isTrue, addr := node.find_successor(prev)
					//if isTrue {
					getSuccessorSlice(ctx, addre)

				}
			case "quit":
				/*isTrue, _ := node.find_successor(joinaddress)
				if isTrue {
					node.put_all(node.Bucket)
				} */
				breakme = true
				break

			//case "next":
			//	log.Print(node.find_successor(joinaddress))

			case "help":
				//log.Print("list of commands: help, quit, port, create, join, dump, put, get, delete")
				fmt.Println()
				fmt.Println()
				fmt.Println("***********************CHORD COMMAND LIST**********************************")
				fmt.Println("help\nlist\nquit\nport\ncreate\njoin\ndump\nput\nget\nping\ndelete\nnext")
				fmt.Println()
				fmt.Println("Showing list of commands (use one of the above to execute an operation)")

			default:
				log.Println("Error, unknown command. Please try again. Type \"help\" for a list of commands")
			}
			if breakme {
				break
			}
		}
		break
	}
	if err := in.Err(); err != nil {
		log.Fatalf("scanning input: %v", err)
	}
	log.Print("quitting")

	if node == nil {
		log.Print("not part of a ring, nothing to clean up")
		return
	}
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/************************CREATE**AND**JOIN*************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

/*func Create(port string, ctx context.Context) (*Node, error) { //Since I am defining create and join seperately, I would assume that create only takes one parameter in this case
	address := net.JoinHostPort(localaddress, port)

	node := &Node{
		Address:     address, //Question: Should this be nprime or just address?
		FingerTable: make([]string, keySize+1),
		Predecessor: "",
		Successors:  nil,
		Bucket:      make(map[string]string),
	}
	log.Print("creating a new ring")
	node.Successors = []string{node.Address}

	grpcServer := grpc.NewServer() //I assume this is where we create the ring
	log.Print("Ring creation successful at ", port)

	pb.RegisterChordServer(grpcServer, node)
	the_error := ping(ctx, port)
	if the_error != nil {
		log.Print("Error, could not ping address")
	} else {
		log.Print("Ping Successful")
	}
	listen, err := net.Listen("tcp", node.Address) //We create the socket for our new node right here

	if err != nil {
		log.Print("Failed to list: %v", err)
		return node, err
	}
	log.Printf("Starting to listen on %v", listen.Addr())

	go func() {
		if err := grpcServer.Serve(listen); err != nil { //I actually do not know what this function is doing
			log.Fatalf("failed to server: %v", err)
		}
	}()

	go func() {
		nextFinger := 0
		time.Sleep(time.Second / 3)
		//stabilizeRPC(port, ctx)
		node.Stabilize(ctx, &pb.StabilizeRequest{Value: address})
		for {

			time.Sleep(time.Second / 3)
			nextFinger = node.fix_fingers(nextFinger)

			time.Sleep(time.Second / 3)
			node.Check_Predecessor(ctx, &pb.PredecessorRequest{})
		}
	}()

	return node, nil

}

func Join(port string, nprime string, ctx context.Context) (*Node, error) {
	address := net.JoinHostPort(localaddress, port)

	fmt.Println(nprime, "nprime")

	node := &Node{
		Address:     address,
		FingerTable: make([]string, keySize+1),
		Predecessor: "",
		Successors:  nil,
		Bucket:      make(map[string]string),
	}
	log.Print("joining existing ring at ", nprime)
	_, nprime, _ = find_successor(node.Address, ctx)
	node.Successors = []string{nprime}
	node.get_all(node.Address, ctx)

	grpcServer := grpc.NewServer()

	pb.RegisterChordServer(grpcServer, node)
	listen, err := net.Listen("tcp", node.Address)

	if err != nil {
		log.Print("Failed to list: %v", err)
		return node, err
	}

	log.Printf("Starting to listen on %v", listen.Addr())

	go func() {
		if err := grpcServer.Serve(listen); err != nil {
			log.Fatalf("failed to server: %v", err)
		}
	}()

	go func() {
		nextFinger := 0
		time.Sleep(time.Second / 3)
		//stabilizeRPC(port, ctx)
		node.Stabilize(ctx, &pb.StabilizeRequest{Value: address}) // I'm not calling RPC on purpose
		for {

			time.Sleep(time.Second / 3)
			nextFinger = node.fix_fingers(nextFinger)

			time.Sleep(time.Second / 3)
			node.Check_Predecessor(ctx, &pb.PredecessorRequest{})

		}
	}()

	return node, nil
} */

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/********************************NILL******************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

func StartServer(port string, nprime string, ctx context.Context) (*Node, error) {
	var address string
	if nprime == "" {
		address = net.JoinHostPort(localaddress, port)

	} else {
		address = net.JoinHostPort(nprime, port)
	}
	fmt.Println(address, "4")

	node := &Node{
		Address:     address,
		FingerTable: make([]string, keySize+1),
		Predecessor: "",
		Successors:  nil,
		Bucket:      make(map[string]string),
	}

	if nprime == "" {
		log.Print("creating a new ring")
		node.Successors = []string{node.Address}
		log.Print("Ring creation successful at ", port)
	} else {
		log.Print("joining existing ring at ", nprime)
		node.Successors = []string{nprime}
	}

	grpcServer := grpc.NewServer()

	pb.RegisterChordServer(grpcServer, node)
	listen, err := net.Listen("tcp", node.Address)

	if err != nil {
		log.Print("Failed to list: %v", err)
		return node, err
	}

	log.Printf("Starting to listen on %v", listen.Addr())

	go func() {
		if err := grpcServer.Serve(listen); err != nil {
			log.Fatalf("failed to server: %v", err)
		}
	}()

	go func() {
		nextFinger := 0
		time.Sleep(time.Second / 3)
		//stabilizeRPC(port, ctx)
		node.Stabilize(ctx, &pb.StabilizeRequest{Value: address}) // I'm not calling RPC on purpose
		for {

			time.Sleep(time.Second / 3)
			nextFinger = node.fix_fingers(nextFinger)

			time.Sleep(time.Second / 3)
			node.Check_Predecessor(ctx, &pb.PredecessorRequest{})

		}
	}()

	return node, nil
}

/*****************************************************************************************************/
/*****************************************************************************************************/
/*****************************************************************************************************/
/*****************************************************************************************************/
/*************************************** NOTIFY ******************************************************/
/*****************************************************************************************************/
/*****************************************************************************************************/
/*****************************************************************************************************/
/*****************************************************************************************************/

func (n *Node) Notify(ctx context.Context, reg *pb.NotifyRequest) (*pb.NotifyResponse, error) {
	//Need to find between successor and predecessor

	if n.Address == reg.Address {
		n.Predecessor = reg.Address
	}

	return &pb.NotifyResponse{Says: n.Predecessor}, nil

}

func notifyRPC(ctx context.Context, myaddr, succ string) { //I put addr here because I am trying to think about which should go into the regular function
	conn, err := grpc.Dial(succ, grpc.WithInsecure())
	fmt.Println(err)
	if err != nil {
		log.Print("Error: Unable to notify")
		return
	}
	defer conn.Close()

	client := pb.NewChordClient(conn)
	//id = net.JoinHostPort(id, "8080")
	ret, _ := client.Notify(ctx, &pb.NotifyRequest{Address: myaddr})
	log.Print("Notify Successful: ", ret)
}

/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/***************************************STABILIZE*******************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/

func (n *Node) Stabilize(ctx context.Context, reg *protocol.StabilizeRequest) (*protocol.StabilizeResponse, error) {

	conn, err := grpc.Dial(n.Successors[0], grpc.WithInsecure())

	if err != nil {
		log.Fatal("No")
	}

	successor := pb.NewChordClient(conn)

	x, _ := successor.Check_Predecessor(ctx, &pb.PredecessorRequest{Address: reg.GetValue()})
	if x.GetValue() == n.Predecessor {
		n.Successors = append(n.Successors, x.GetValue())

	} else {
		n.Successors = n.Successors[:len(n.Successors)-1]
	}

	notifyRPC(ctx, n.Address, x.GetValue())

	var temp *protocol.StabilizeResponse

	return temp, nil
}

func (n *Node) Check_Predecessor(ctx context.Context, in *pb.PredecessorRequest) (*pb.PredecessorResponse, error) {
	if n.Predecessor == "" {
		return &pb.PredecessorResponse{Value: localaddress}, nil
	}
	return &pb.PredecessorResponse{Value: n.Predecessor}, nil

}

func stabilizeRPC(addr string, ctx context.Context) error {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer conn.Close()

	client := pb.NewChordClient(conn)

	_, err = client.Stabilize(ctx, &pb.StabilizeRequest{Value: addr})

	return err

}

/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/****************************************INIT***********************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/

// func getLocalAddress() string {
func init() {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	localAddr := conn.LocalAddr().(*net.UDPAddr)
	localaddress = localAddr.IP.String()

	if localaddress == "" {
		panic("init: failed to find non-loopback interface with valid address on this node")
	}

	log.Printf("found local address %s\n", localaddress)
}

/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/******************************************FIND*********************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/

func (n *Node) Find(ctx context.Context, req *pb.FindRequest) (*pb.FindResponse, error) {
	n.mu.Lock()
	defer n.mu.Unlock()
	max := 20
	//nextNode := start
	found, nextNode := false, req.Address
	fmt.Println(nextNode)
	i := 0
	for !found && i < max {

		found, nextNode = Successor(nextNode, req.Address, n)
		i++
	}
	if found {
		return &pb.FindResponse{Address: nextNode}, nil
	} else {
		return &pb.FindResponse{}, nil
	}
}

func Successor(current, target string, n *Node) (bool, string) {
	for i := 0; i < len(n.Successors); i++ {
		if n.Successors[i] == current {
			current = n.Successors[i]
			if current == target {
				return true, current
			} else {
				current = n.Successors[i+1]
			}
		}
		return false, current
	}
	return false, "No node found"
}

func find_successor(id string, ctx context.Context) (error, string, bool) {
	conn, err := grpc.Dial(id, grpc.WithInsecure())
	if err != nil {
		log.Println("Error: Failed to list successors")
		return err, id, false
	}
	defer conn.Close()

	client := pb.NewChordClient(conn)

	fmt.Println(client)

	new_string, new_err := client.Find(ctx, &pb.FindRequest{})

	fmt.Println(new_string)
	fmt.Println(new_err)

	return new_err, new_string.Address, true

}

func find_RPC(addr string, ctx context.Context) error {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer conn.Close()

	client := pb.NewChordClient(conn)

	_, err = client.Find(ctx, &pb.FindRequest{Address: addr})
	return err

}

/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/****************************************FIX*FINGERS****************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/

func (n *Node) fix_fingers(nextFinger int) int {
	nextFinger++
	if nextFinger > keySize {
		nextFinger = 1
	}
	return nextFinger
}

/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/***************************************GET\PUT*&*ADDR**************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/

func addr(the_string string) string {
	if the_string == "" {
		return "(empty)"
	}
	s := fmt.Sprintf("%040x", hashString(the_string))
	return s[:8] + ".. (" + the_string + ")"
}

func (n *Node) get_all(addr string, ctx context.Context) map[string]string {

	new_bucket := make(map[string]string)
	_, new_addr, inclusive := find_successor(addr, ctx)

	start := new(big.Int)
	elt := new(big.Int)
	end := new(big.Int)

	start, _ = start.SetString(localaddress, 10)
	elt, _ = elt.SetString(addr, 10)
	end, _ = end.SetString(new_addr, 10)

	isTrue := between(start, elt, end, inclusive)

	if isTrue {
		for key, value := range n.Bucket {
			new_bucket[key] = value
			delete(n.Bucket, key)
		}

		return new_bucket

	} else {
		log.Print("Error, could not find that node between")
		return new_bucket
	}
	//var temp map[string]string
	//return temp

}

func (n *Node) put_all(b map[string]string) {
	for key, value := range b {
		n.Bucket[key] = value
	}

}

/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/********************************************HASH FUNCTIONS*********************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/

func hashString(elt string) *big.Int {
	hasher := sha1.New()
	hasher.Write([]byte(elt))
	return new(big.Int).SetBytes(hasher.Sum(nil))
}

func jump(address string, fingerentry int) *big.Int {
	n := hashString(address)
	fingerentryminus1 := big.NewInt(int64(fingerentry) - 1)
	jump := new(big.Int).Exp(two, fingerentryminus1, nil)
	sum := new(big.Int).Add(n, jump)

	return new(big.Int).Mod(sum, hashMod)
}

func between(start, elt, end *big.Int, inclusive bool) bool {
	if end.Cmp(start) > 0 {
		return (start.Cmp(elt) < 0 && elt.Cmp(end) < 0) || (inclusive && elt.Cmp(end) == 0)
	} else {
		return start.Cmp(elt) < 0 || elt.Cmp(end) < 0 || (inclusive && elt.Cmp(end) == 0)
	}
}

/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/****************************************MAIN FUNCTION**************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/
func main() {
	log.SetFlags(log.Ltime | log.Lmicroseconds)
	var create, doshell bool
	var port, joinaddress string
	var dumpinterval int
	var notime bool

	flag.BoolVar(&notime, "notime", notime, "Omit timestamp at the beginning of each output line")
	flag.StringVar(&port, "port", defaultPort, "Set the port for this node at address "+localaddress)
	flag.BoolVar(&create, "create", false, "Create a new ring")
	flag.StringVar(&joinaddress, "join", "", "Join an existing ring at given address (implies -create=false)")
	flag.IntVar(&dumpinterval, "dump", -1, "Dump status info every n seconds (<1 implies no priodic dumps)")
	flag.BoolVar(&doshell, "shell", true, "Start an interactive shell (implies dump = -1)")
	flag.Parse()

	if notime {
		log.SetFlags(0)
	}

	if joinaddress != "" {
		create = false
	}
	if doshell {
		dumpinterval = -1
	}

	rand.Seed(time.Now().UnixNano())

	log.Print("Chord Implementation - Part 1 (and part 1 only)")

	log.Println()

	var node *Node
	var err error

	if create || joinaddress != "" {

		if create || joinaddress != "" {
			if strings.HasPrefix(joinaddress, ":") {
				joinaddress = net.JoinHostPort(joinaddress, joinaddress[1:])
			} else if joinaddress != "" && !strings.Contains(joinaddress, ":") {
				joinaddress = net.JoinHostPort(joinaddress, port)
			}
			ctx := context.Background()
			if node, err = StartServer(port, joinaddress, ctx); err != nil {
				//if node, err = Join(port, joinaddress, ctx); err != nil {
				log.Fatal("Failed creating node: ", err)
			}

		}
	}
	if dumpinterval >= 1 {

		go func() {
			for {
				ctx := context.Background()
				time.Sleep(time.Second * time.Duration(dumpinterval))
				dumpRPC(joinaddress, ctx)
			}
		}()
	}
	if doshell {

		shell(node, port, joinaddress)
	} else {

		log.Print("No interactive shell, main goroutine going to sleep")
		for {
			time.Sleep(time.Minute)
		}
	}

}
